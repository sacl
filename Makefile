saclroot=.

include $(saclroot)/common.mk

all: saclc

saclc:
	$(SILENT) $(MAKE) -C $(COMPILERDIR) saclc
	$(SILENT) cp -f $(COMPILERDIR)/saclc ./

installdirs:
	$(SILENT) $(INSTALL) -d $(exec_prefix) $(lispdir) $(includedir) $(datadir) $(man1dir)

install: saclc installdirs
	$(info Installing) # TODO: Plugins and documentation missing
	$(SILENT) $(INSTALL) saclc

backup: superclean
	tar  '--exclude=*.git*' -jcvf ../sacl-backup.tar.bz2 .

superclean: clean
	rm -f core saclc

compilerclean:
	$(MAKE) -C src/compiler clean
	rm -f saclc

clean: compilerclean


tags: TAGS


.PHONY: clean superclean backup compilerclean
