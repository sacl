##
# Variables included by all Makefiles.
##

## Directory definitions

libdir       = $(saclroot)/lib
mobiusdir    = $(saclroot)/src/mobius
pluginsdir   = $(saclroot)/src/plugins
testdir      = $(saclroot)/tests

prefix      = /usr/local
datadir     = $(prefix)/share/sacl
exec_prefix = $(prefix)/bin
execname    = sacl
includedir  = $(prefix)/include
mandir      = $(prefix)/share/man
man1dir     = $(mandir)/man1
infodir     = $(prefix)/info
lispdir     = $(prefix)/share/emacs/site-lisp
DEVNULL     := /dev/null

## Build modes. Set on command line using QUIET=1, DEBUG=1 or PROFILE=1

ifndef QUIETNESS
  QUIETNESS := -q
endif

ifdef QUIET
  VERBOSE := 0
  SILENT := @
  SILENT_ERRORS := 2> $(DEVNULL)
  LIBTOOL_FLAGS += --silent
endif

ifdef DEBUG
  MODE := debug
  COPTFLAGS += -g
  LDFLAGS += -g
  CFLAGS += -DSACL_BUILD_TYPE="\"Debug\""
else
  ifdef PROFILE
    MODE += profiled
    COPTFLAGS += -pg -g -O2 -fprofile-arcs -ftest-coverage
    LDFLAGS += -pg -g -fprofile-arcs -ftest-coverage
    CFLAGS += -DSACL_BUILD_TYPE="\"Profile\""
  else
    MODE := optimized
    CFLAGS += -DSACL_BUILD_TYPE="\"Optimized\""
    COPTFLAGS += -O2
  endif
endif

## All required executables

CC          := gcc
CPP         := g++
CP          := cp -f
LIBTOOL     ?= libtool
ECHO        := echo
WGET        := wget -q --cache=off
SECHO       := @$(ECHO)
INSTALL     := install
TAR         := tar
GZIP        := gzip
ETAGS       := etags
EMACS       := emacs

## Platform independent definitions

INCLUDES    += 
#CFLAGS      += -DSACL_DATADIR=$(datadir) -D_POSIX_SOURCE=200112L -D_POSIX_C_SOURCE=200112L
CFLAGS      += -DSACL_DATADIR=$(datadir) -D_XOPEN_SOURCE=600 --std=c++0x
CFLAGS      += $(COPTFLAGS) -Wall -Wno-unknown-pragmas $(EXTRACFLAGS)  $(INCLUDES)
# include -pedantic later fixme

## Determine the host system's byte order.
## This creates a temporary test executable in the $(saclroot) directory
## since we can guarantee write permissions there on all platforms. 

BYTE_ORDER_FN := $(saclroot)/byteorder
BYTE_ORDER_SRC  := "int main(){union{long l;char c[sizeof(long)];}u;u.l=1;return(u.c[sizeof(long)-1]==1);}"
BYTE_ORDER  := $(shell echo $(BYTE_ORDER_SRC) > $(BYTE_ORDER_FN).c)
BYTE_ORDER  := $(shell $(CC) -o $(BYTE_ORDER_FN) $(BYTE_ORDER_FN).c)
BYTE_ORDER  := $(shell $(BYTE_ORDER_FN); echo $$?)
BYTE_ORDER_ := $(shell $(RM) $(BYTE_ORDER_FN).* $(BYTE_ORDER_FN) 1>&2)
ifeq ($(BYTE_ORDER),0)
  BYTE_ORDER := LITTLE_ENDIAN
  BYTE_ORDER_PREFIX := little
  LITTLE_ENDIAN_SACL := True
else
  BYTE_ORDER := BIG_ENDIAN
  BYTE_ORDER_PREFIX := big
  LITTLE_ENDIAN_SACL := False
endif

## Default variables

PLATFORM    := unix
CCVERSION   := $(shell $(CC) -dumpversion)
LDFLAGS     := 
LIBS        := #-lm -ldl
HOST_SYSTEM := $(shell uname -s)
LIB_SO_EXT  := .so
INSTALL_MODE := -m 644
CPU_TYPE    := `uname -m`

COMPILERDIR       := $(saclroot)/src/compiler


## Determine CPU type

## TODO: Sparc detection for SunOS?
## TODO: Base CPU type on real information, not just generic OS variant

## CPU-type specific overrides. Any of the variables above may be changed here.

ifdef ARCH
  CFLAGS += -m$(ARCH)
endif

ifdef VERSION
  CFLAGS += -DVERSION="\"$(VERSION)\""
endif

ifdef WORD_SIZE
  CFLAGS += -m$(WORD_SIZE)
  CFLAGS += -D_FILE_OFFSET_BITS=$(WORD_SIZE)
endif


## Platform specific overrides. Any of the variables above may be changed here.


ifeq ($(findstring CYGWIN,$(HOST_SYSTEM)), CYGWIN)
  PLATFORM   := windows
  LDFLAGS    += -no-undefined
  LIBS       := -lm
  LIB_SO_EXT := .dll
# DEVNULL    := NUL # Required if using cmd.exe and not bash.
endif

ifeq ($(HOST_SYSTEM), Darwin)
#  LIBTOOL := MACOSX_DEPLOYMENT_TARGET=10.3 glibtool
  LIBTOOL := glibtool
  PLUGINS := cocoa-windows.so
endif

ifeq ($(HOST_SYSTEM), DragonFly)
  LIBS      := -lm
endif

ifeq ($(HOST_SYSTEM), FreeBSD)
  LIBS      := -lm -lc_r
  LIBTOOL  := libtool13
endif

ifeq ($(HOST_SYSTEM), HP-UX)
  LIBS       := -lm
# -ldld
  LIB_SO_EXT := .sl
endif

ifeq ($(HOST_SYSTEM), Linux)
  LIBS       := -lm
# -ldl
endif

ifeq ($(findstring MINGW,$(HOST_SYSTEM)), MINGW)
  PLATFORM  := windows
  LIBS      := -lm
endif

ifeq ($(HOST_SYSTEM), NetBSD)
  LIBS      := -lm
endif

PLUGINS := $(subst .so,$(LIB_SO_EXT), $(PLUGINS))


