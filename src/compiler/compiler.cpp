
#include "compiler.hpp"

Compiler::Compiler() : internCounter(1) {
}

Compiler::~Compiler() {
}

Compiler::Symbol Compiler::intern(const std::string& str) {
  Symbol val = symbolTable[str];
  if (val == 0) {
    symbolTable[str] = internCounter;
    return internCounter++;
  }
  return val;
}


void Compiler::compile(SyntaxNodePtr ptr) {
  if (ptr.get() == /*nullptr*/ NULL) {
    std::cerr << "Error: Cannot compile because of previous errors" << std::endl;
  } else {
    ptr->compile(this);
  }
}

void SyntaxNode::compile(Compiler* c) {
  //fixme
  std::cerr << "Fixme" << std::endl;

}

void ObjectDefinition::compile(Compiler* c) {
  c->compile(this);
}


