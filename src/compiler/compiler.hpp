
#pragma once

#include "parser.hpp"
#include <iostream>
#include <map>
#include <string>

class Compiler {
public:
  typedef int Symbol;
  Symbol intern(const std::string& str);

private:
  std::map<const std::string, Symbol> symbolTable;
  int internCounter;

public:
  virtual void compile(SyntaxNodePtr node);
  Compiler();
  virtual ~Compiler();

  virtual void compile(ObjectDefinition* od) = 0;
  virtual void writeOutput(std::ostream& os) = 0;

};
