
#include "cppcompiler.hpp"

CPPCompiler::CPPCompiler() : methodIndex(1) {
  intern("Program");
  intern("start");
}

CPPCompiler::~CPPCompiler() {
}

std::string& CPPCompiler::newMethodName() {
  std::ostringstream oss;
  oss << "method_" << methodIndex++;
  return oss.str();
}

void CPPCompiler::compile(ObjectDefinition* od) {

  Symbol odName = intern(od->name);
  for (auto m = od->methods.begin(); m != od->methods.end(); m++) {
    Symbol methodName = intern((*m)->name);
    std:: cout << "method: " << methodName << std::endl;
  }
}


void CPPCompiler::writeOutput(std::ostream& os) {
  os << "Hello";

}
