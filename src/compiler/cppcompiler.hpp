
#pragma once

#include "compiler.hpp"
#include <sstream>

struct CPPClass {
  
};


class CPPCompiler : public Compiler {

  int methodIndex;

public:

  std::string newMethodName();
  CPPCompiler();
  ~CPPCompiler();
  virtual void compile(ObjectDefinition* od);
  virtual void writeOutput(std::ostream& os);


};
