
#include "llvmcompiler.hpp"

LLVMCompiler::LLVMCompiler() : internCounter(1) {
}

LLVMCompiler::~LLVMCompiler() {
}


Symbol intern(const string& str) {
  Symbol val = symbolTable.at(str);
  if (val == 0) {
    symbolTable[str] = internCounter;
    return internCounter++;
  }
  return val;
}

void LLVMCompiler::compile(ObjectDefinition* od) {

  Symbol odName = intern(od->name);
  for (auto m = od->methods.begin(); m != od->methods.end(); m++) {
    Symbol methodName = intern(m->name);
  }
}


