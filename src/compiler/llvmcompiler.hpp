
#pragma once

#include "compiler.hpp"

class LLVMCompiler : public Compiler {


  std::map<const string&, Symbol> symbolTable;
  int internCounter;

public:

  typedef int Symbol;

  Symbol intern(const string& str);

  LLVMCompiler();
  ~LLVMCompiler();
  virtual void compile(ObjectDefinition* od);

};
