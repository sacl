
#include "parser.hpp"

void SyntaxNode::setPosition(Token& t) {
  line = t.line;
  col = t.col;
}
SyntaxNode::SyntaxNode() : line(0), col(0) {

}

SyntaxNode::~SyntaxNode() {

}


Parser::Parser(Tokenizer& t) : tokenizer(t), errors(0) {

}

Token Parser::readToken() {
  return tokenizer.readToken();
}

void Parser::pushToken(Token& t) {
  tokenizer.pushToken(t);
}


void Parser::printToken(Token& t) {
  std::cerr << "<Token Type " << t.type << ", Line: " << t.line << ":" << t.col
            << "), text: '" << t.text.c_str() << "' >";
}

void Parser::expectedDifferentToken(Token& t, const char* errorString) {
  errors++;
  std::cerr << "Error: expected token '" << errorString << "' but found ";
  printToken(t);
  std::cerr << std::endl;
}

void Parser::assertToken(Token& t, enum Token::TokenType tt, const char* msg) {
  if (t.type != tt) {
    expectedDifferentToken(t, msg);
    for (Token skip = readToken(); skip.type != tt && skip.type != Token::EndOfFile; skip = readToken());
  }
}

ObjectDefinitionPtr Parser::readObjectDefinition() {
  auto def = std::make_shared<ObjectDefinition>();
  Token t = readToken();
  assertToken(t, Token::ObjectDefinition, "obj"); 

  Token objName = readToken();
  assertToken(objName, Token::Identifier, "object name");
  
  t = readToken();
  assertToken(t, Token::LeftParen, "(");
  
  NodeList inheritanceList, slotList;
  inheritanceList = readIdentifierList();

  t = readToken();
  assertToken(t, Token::RightParen, ")");
  t = readToken();
  if (t.type == Token::LeftBrace) {
    for (t = readToken(); t.type == Token::MethodDefinition; t = readToken()) {
      pushToken(t);
      def->methods.push_back(readMethod());
    }
    assertToken(t, Token::RightBrace, "}");
  } else if (t.type == Token::Colon) {
    slotList = readIdentifierList();
  } else {
    assertToken(t, Token::LeftBrace, "{");
  }
  
  return def;
}

MethodDefinitionPtr Parser::readMethod() {
  auto def = make_shared<MethodDefinition>();
  Token t = readToken();
  assertToken(t, Token::MethodDefinition, "def");
  t = readToken();
  assertToken(t, Token::Identifier, "<identifier>");
  def->name = t.identifier;
  
  t = readToken();
  assertToken(t, Token::LeftParen, "(");
  
  def->parameters = readIdentifierList();

  t = readToken();
  assertToken(t, Token::RightParen, ")");

  t = readToken();
  if (t.type == Token::LeftBrace) {
    for (t = readToken(); t.type != Token::RightBrace && t.type != Token::EndOfFile; t = readToken()) {
      pushToken(t);
      def->statements.push_back(readStatement());
    }

    assertToken(t, Token::RightBrace, "}");
  } else if (t.type == Token::Colon) {
    t = readToken();
    assertToken(t, Token::Identifier, "<identifier delegate name>");
  } else {
    assertToken(t, Token::LeftBrace, "{");
  }

  return def;
}

StatementPtr Parser::readStatement() {
  auto s = make_shared<Statement>();
  Token t = readToken();
  switch (t.type) {
  case Token::Caret: 
    {
      auto rs = new ReturnStatement;
      rs->setPosition(t);
      rs->expression = readExpression();
      return StatementPtr(static_cast<Statement*>(rs));
    }
#if 0
  case Token::IfToken:
    std::cerr << "Fixme" << std::endl;
    break;
  case Token::WhileToken:
    std::cerr << "Fixme" << std::endl;
    break;
  case Token::ForToken:
    std::cerr << "Fixme" << std::endl;
    break;
#endif
  default:
    {
      auto rs = new PlainStatement;
      rs->setPosition(t);
      pushToken(t);
      rs->expression = readExpression();
      t = readToken();
      assertToken(t, Token::Semicolon, ";");
      return StatementPtr(static_cast<Statement*>(rs));
    }
  }
}


ExpressionPtr Parser::readSimpleExpression() {
  Token t = readToken();
  switch (t.type) {
  case Token::StringLiteral:
    {
      auto val = new StringLiteral;
      val->setPosition(t);
      val->value = t.identifier;
      return ExpressionPtr(static_cast<Expression*>(val));
    }
  case Token::IntegerLiteral:
    std::cerr << "Fixme" << std::endl;
    {
      auto val = new IntegerLiteral;
      val->setPosition(t);
      return ExpressionPtr(static_cast<Expression*>(val));
    }
  case Token::LambdaDefinition:
    std::cerr << "Fixme" << std::endl;
    {
      auto val = new IntegerLiteral;
      val->setPosition(t);
      return ExpressionPtr(static_cast<Expression*>(val));
    }
  case Token::LeftParen:
    {
      ExpressionPtr e = readExpression();
      t = readToken();
      assertToken(t, Token::RightParen, ")");
      return e;
    }
  case Token::Identifier:
    {
      auto val = new LoadVariable;
      val->setPosition(t);
      val->name = t.identifier;
      return ExpressionPtr(static_cast<Expression*>(val));
    }
    //    break;
  default:
    assertToken(t, Token::Identifier, "expression");
    {
      auto val = new IntegerLiteral;
      val->setPosition(t);
      return ExpressionPtr(static_cast<Expression*>(val));
    }
  }
}

ExpressionPtr Parser::readUnaryOperation() {

  ExpressionPtr e = readSimpleExpression();
  Token t = readToken();
  if (t.type == Token::Period) {
    auto msg = new MessageSend;
    t = readToken();
    assertToken(t, Token::Identifier, "method name");
    msg->setPosition(t);
    msg->firstArgument = e;
    msg->messageName = t.identifier;
    t = readToken();
    assertToken(t, Token::LeftParen, "(");
    msg->arguments = readExpressionList();
    t = readToken();
    assertToken(t, Token::RightParen, ")");
    return ExpressionPtr(msg);
  } else {
    pushToken(t);
    return e;
  }
}

ExpressionPtr Parser::readExpression() {

  auto e = readUnaryOperation();
  Token t = readToken();
  if (t.type == Token::BinaryOperation) {
    auto ret = new BinaryOperation;
    ret->setPosition(t);
    ret->op = t.identifier;
    ret->lhs = e;
    ret->rhs = readExpression();
    return ExpressionPtr(static_cast<Expression*>(ret));
  } else {
    pushToken(t);
    return e;
  }
}



NodeList Parser::readIdentifierList() {
  NodeList list;
  Token t = readToken();

  if (t.type == Token::RightParen) {
    pushToken(t);
    return list;
  } else if (t.type == Token::Identifier) {
    StringLiteral* sl = new StringLiteral;
    sl->setPosition(t);
    sl->value = t.identifier;
    auto id = shared_ptr<SyntaxNode>(static_cast<SyntaxNode*>(sl));
    list.push_back(id);
  }

  do {
    t = readToken();
    if (t.type == Token::RightParen) {
      pushToken(t);
      break;
    }
    assertToken(t, Token::Comma, ",");
    t = readToken();
    if (t.type == Token::Identifier) {
      StringLiteral* sl = new StringLiteral;
      sl->setPosition(t);
      sl->value = t.identifier;
      auto id = shared_ptr<SyntaxNode>(static_cast<SyntaxNode*>(sl));
      list.push_back(id);
    } else {
      assertToken(t, Token::Identifier, "<identifier>");
    }
  } while (t.type != Token::EndOfFile);

  return list;
}


ExpressionList Parser::readExpressionList() {
  ExpressionList list;
  Token t = readToken();

  if (t.type == Token::RightParen) {
    pushToken(t);
    return list;
  } else {
    pushToken(t);
    list.push_back(readExpression());
  }

  do {
    t = readToken();
    if (t.type == Token::RightParen) {
      pushToken(t);
      break;
    }
    assertToken(t, Token::Comma, ",");
    list.push_back(readExpression());
    
  } while (t.type != Token::EndOfFile);

  return list;
}

bool Parser::parse() {
  ObjectDefinitionPtr od = readObjectDefinition();
  //root = make_shared<SyntaxNode>(static_cast<SyntaxNode*>(od.get()));;
  root = shared_ptr<SyntaxNode>(od);
  return errors <= 0;
}

SyntaxNodePtr Parser::getRoot() {
  return root;
}

