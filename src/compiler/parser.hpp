#pragma once

#include "tokenizer.hpp"
#include <memory>
#include <utility>


using std::make_shared;
using std::shared_ptr;
class Compiler;

struct SyntaxNode {
  int line, col;
  SyntaxNode();
  virtual void setPosition(Token&);
  virtual ~SyntaxNode();
  virtual void compile(Compiler* c);
};

typedef std::shared_ptr<SyntaxNode> SyntaxNodePtr;
typedef std::vector<SyntaxNodePtr> NodeList;

class Expression : public SyntaxNode {};
typedef shared_ptr<Expression> ExpressionPtr;
typedef std::vector<ExpressionPtr> ExpressionList;


struct StringLiteral : public Expression {std::string value;};
struct IntegerLiteral : public Expression {};
struct LambdaLiteral : public Expression {};
struct BinaryOperation : public Expression {ExpressionPtr lhs, rhs; std::string op;};
struct MessageSend : public Expression {ExpressionPtr firstArgument;
  std::string messageName; ExpressionList arguments;};
struct LoadVariable : public Expression {std::string name;};

struct Statement : public SyntaxNode {
};

struct PlainStatement : public Statement {
  ExpressionPtr expression;
};

struct ReturnStatement : public Statement {
  ExpressionPtr expression;
};

typedef shared_ptr<Statement> StatementPtr;

struct MethodDefinition : public SyntaxNode {
  std::string name, delegateTo;
  NodeList parameters;
  NodeList statements;
};

typedef shared_ptr<MethodDefinition> MethodDefinitionPtr;
typedef std::vector<MethodDefinitionPtr> MethodList;


struct ObjectDefinition : public SyntaxNode {
  MethodList methods;
  std::string name;
  virtual void compile(Compiler* c);
};

typedef shared_ptr<ObjectDefinition> ObjectDefinitionPtr;

class Parser {

  Tokenizer& tokenizer;
  Parser();
  int errors;
  SyntaxNodePtr root;

public:

  Parser(Tokenizer& t);
  bool parse();

  Token readToken();
  void pushToken(Token& t);
  void expectedDifferentToken(Token&, const char*);
  void printToken(Token&);
  void assertToken(Token&, enum Token::TokenType, const char*);

  NodeList readIdentifierList();
  ExpressionList readExpressionList();
  SyntaxNodePtr getRoot();

  ObjectDefinitionPtr readObjectDefinition();
  MethodDefinitionPtr readMethod();
  StatementPtr readStatement();
  ExpressionPtr readExpression();
  ExpressionPtr readUnaryOperation();
  ExpressionPtr readSimpleExpression();

};
