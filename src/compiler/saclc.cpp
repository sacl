
#include <iostream>
#include <fstream>
#include "parser.hpp"
#include "cppcompiler.hpp"

void compileFile(char* filename) {
  
  std::ifstream ifs(filename, std::ios::binary);
  if (!ifs) {
    std::cerr << "Cannot open '" << filename << "'" << std::endl;
  } else {
    Tokenizer t(ifs);
    Parser p(t);
    if (p.parse()) {
      CPPCompiler lc;
      p.getRoot()->compile(&lc);
      lc.writeOutput(std::cout);
    }
  }
}


int main(int argc, char* argv[]) {

  for (int i = 1; i < argc; i++) {
    compileFile(argv[i]);
  }
  return 0;
}
