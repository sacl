
#include "tokenizer.hpp"
#include "ctype.h"

Token::Token(int line_, int col_, std::string& txt, TokenType t)
  : line(line_), col(col_), type(t), text(txt) {}
Token::Token(int line_, int col_, std::string& txt, TokenType t, std::string& id)
  : line(line_), col(col_), type(t), identifier(id), text(txt) {}

Tokenizer::Tokenizer(std::istream& i) : stream(i), line(1), col(0) {}

bool Tokenizer::isWhitespace(char c) {
  return isspace(c);
}

std::istream& Tokenizer::getChar(char& c) {
  std::istream& res = stream.get(c);
  if (c == '\n') {
    line++;
    lastCol = col;
    col = 0;
  } else {
    col++;
  }
  currentText += c;
  return res;
}

Token Tokenizer::makeToken(Token::TokenType type) {
  std::string text = currentText;
  currentText = "";
  return Token(line, col, text, type);
}

Token Tokenizer::makeToken(Token::TokenType type, std::string& str) {
  std::string text = currentText;
  currentText = "";
  return Token(line, col, text, type, str);
}

void Tokenizer::putBack(char c) {
  if (c == '\n') {
    line--;
    col = lastCol;
  } else {
    col--;
  }
  if (!currentText.empty()) {
    currentText.resize(currentText.size()-1);
  }
  stream.putback(c);
}


void Tokenizer::skipWhitespace() {
  char c;
  while (getChar(c)) {
    if (!isWhitespace(c)) {
      putBack(c);
      return;
    }
  }
}

void Tokenizer::skipComment() {
  char c;
  while (getChar(c)) {
    if (c == '\n') {
      return;
    }
  }
}

std::string Tokenizer::readIdentifier() {
  std::string str;
  char c;
  while (getChar(c)) {
    if (!isalnum(c)) {
      putBack(c);
      break;
    } else {
      str += c;
    }
  }
  return str;

}

Token Tokenizer::readString(char startChar) {
  std::string contents;
  char c;
  while (getChar(c)) {
    if (c == startChar) break;
    contents += c;
  }
  return makeToken(Token::StringLiteral, contents);
}

Token Tokenizer::readTokenFromStream() {
  char c;
  if (!getChar(c)) {
    return makeToken(Token::EndOfFile);
  }

  if (isWhitespace(c)) {
    skipWhitespace();
    currentText = "";
    return readTokenFromStream();
  }
  switch (c) {
  case '{': return makeToken(Token::LeftBrace);
  case '}': return makeToken(Token::RightBrace);
  case '(': return makeToken(Token::LeftParen);
  case ')': return makeToken(Token::RightParen);
  case ',': return makeToken(Token::Comma);
  case ':': return makeToken(Token::Colon);
  case '.': return makeToken(Token::Period);
  case ';': return makeToken(Token::Semicolon);
  case '^': return makeToken(Token::Caret);
  case '"': return readString(c);
  case '#': {skipComment(); return readTokenFromStream();}
  default:
    break;
  }

  if (isalnum(c)) {
    
    std::string s;
    s += c;
    s += readIdentifier();
    if (s == "obj") {
      return makeToken(Token::ObjectDefinition);
    } else if (s == "def") {
      return makeToken(Token::MethodDefinition);
    } else if (s == "if") {
      return makeToken(Token::IfToken);
    } else if (s == "else") {
      return makeToken(Token::ElseToken);
    } else if (s == "while") {
      return makeToken(Token::WhileToken);
    } else if (s == "for") {
      return makeToken(Token::ForToken);
    } else {
      return makeToken(Token::Identifier, s);
    }
    
  }

  std::cerr << "Fixme: " << c <<  std::endl;
  return makeToken(Token::EndOfFile);
}

Token Tokenizer::readToken() {
  if (tokens.empty()) {
    return readTokenFromStream();
  } else {
    Token tp = tokens.front();
    tokens.pop();
    return tp;
  }
}

void Tokenizer::pushToken(Token& tp) {
  tokens.push(tp);
}
