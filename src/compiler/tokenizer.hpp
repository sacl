
#pragma once

#include <iostream>
#include <queue>
//#include <memory>

struct Token {
  enum TokenType {Whitespace, Comment, MethodDefinition, ObjectDefinition, Caret,
                  Identifier, Comma, Colon, Period, Semicolon,
                  StringLiteral, LeftParen, RightParen, LeftBrace, RightBrace,
                  IfToken, ElseToken, WhileToken, ForToken, IntegerLiteral,
                  LambdaDefinition, BinaryOperation, EndOfFile};
  int line, col;
  TokenType type;
  std::string identifier;
  std::string text;

  Token(int, int, std::string&, TokenType t);
  Token(int, int, std::string&, TokenType t, std::string& identifier);
};

//typedef std::shared_ptr<Token> TokenPointer;

class Tokenizer {

  Tokenizer();
  std::istream& stream;
  std::queue<Token> tokens;
  int line, col, lastCol;
  std::string currentText;
  

public:

  Tokenizer(std::istream&);
  Token readToken();
  void pushToken(Token&);
  Token readTokenFromStream();
  bool isWhitespace(char c);
  void skipWhitespace();
  void skipComment();
  std::string readIdentifier();

  std::istream& getChar(char& c);
  void putBack(char c);
  Token makeToken(Token::TokenType type);
  Token makeToken(Token::TokenType type, std::string&);
  Token readString(char startChar);

};
