

#include "array.hpp"

#include <string.h>

Object* method_byte_array_size_0(struct Object* obj) {
  //char* c = objectByteStart(obj);
  return 0; // Console.writeLine("blah")
}

Object* method_object_array_size_0(struct Object* obj) {
  printf("fixme\n");
  return 0; // Console.writeLine("blah")
}





const ObjectVTable* get_byte_array_vt() {
  return &byte_array_vt;
}

const ObjectVTable* get_object_array_vt() {
  return &object_array_vt;
}

Object* createByteArray(int len) {
  Object* obj = makeObject(0, len, &byte_array_vt);
  memset(objectPayloadStart(obj), 0, len);
  return obj;
}

Object* createObjectArray(int len) {
  Object* obj = makeObject(len, 0, &object_array_vt);
  for (auto s = obj->slotBegin(); s != obj->slotEnd(); s++) {
    *s = get_null_object();
  }
  return obj;
}


const ObjectVTable byte_array_vt = {{NULL, NULL, NULL, NULL, NULL, (void*)method_byte_array_size_0, NULL}};
const ObjectVTable object_array_vt = {{NULL, NULL, NULL, NULL, NULL, (void*)method_object_array_size_0, NULL}};
