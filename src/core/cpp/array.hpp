#pragma once
#include "base.hpp"


extern const ObjectVTable object_array_vt;
extern const ObjectVTable byte_array_vt;


const ObjectVTable* get_byte_array_vt();
const ObjectVTable* get_object_array_vt();

Object* createObjectArray(int size);
Object* createByteArray(int size);

