#include "base.hpp"
#include "string.hpp"
#include "int.hpp"
#include <stdlib.h>

__thread Heap* heap = NULL;
__thread Object* nullObject = NULL;

const ObjectVTable null_vt = {{NULL, NULL, NULL, NULL, NULL}};

void* allocateStorage(int byteCount) {
  return calloc(1, byteCount);
}

Object* allocateObject(int slotCount, int byteCount) {
  Object* obj = static_cast<Object*>(allocateStorage(sizeof(struct Object) + slotCount * sizeof(struct Object*) + byteCount));
  obj->pinCount = 0;
  obj->slotCount = slotCount;
  obj->byteCount = byteCount;
  return obj;
}


VTable* makeVTable(int fnCount) {
  VTable* vt = static_cast<VTable*>(allocateStorage(fnCount * sizeof(struct Object*)));
  return vt;
}

Object* makeObject(int slotCount, int byteCount, const ObjectVTable* vt) {
  Object* obj = static_cast<Object*>(allocateObject(slotCount, byteCount));
  obj->vt = vt;
  return obj;
}

#include <stdio.h>
Object* callMethod(struct Object* obj, int internNumber) {
  if (obj == NULL) {
    printf("called %d on null obj\n", internNumber);
    return NULL;
  }
  ZeroArgumentMethod m = reinterpret_cast<ZeroArgumentMethod>(obj->vt->functions[internNumber]);
  return m(obj);
}
Object* callMethod(struct Object* obj, int internNumber, struct Object* arg1) {
  if (obj == NULL) {
    printf("called %d on null obj\n", internNumber);
    return NULL;
  }
  OneArgumentMethod m = reinterpret_cast<OneArgumentMethod>(obj->vt->functions[internNumber]);
  return m(obj, arg1);
}


const ObjectVTable* get_null_vt() {
  return &null_vt;
}

Object* method_null(struct Object* root) {
  return root->slots[rootslot::null];
}

Object* get_null_object() {
  return heap->rootObject->slots[rootslot::null];
}


void start() {
  callMethod(callMethod(heap->rootObject, symbol::Program_0), symbol::Start_0);

}

char* objectByteStart(struct Object* obj) {
  return reinterpret_cast<char*>(obj->slots[obj->slotCount]);
}


void init_base(Heap* heap) {
  heap->rootObject->slots[rootslot::string] = makeObject(0, 0, get_string_vt());
  heap->rootObject->slots[rootslot::integer] = makeObject(0, 0, get_literal_integer_vt()); 
  heap->rootObject->slots[rootslot::null] = makeObject(0, 0, get_null_vt());
}

// in generated program
extern void init_heap(Heap*);

int main(int argc, char* argv[]) {
  heap = new Heap();
  init_heap(heap);
  start();
  return 0;
}
