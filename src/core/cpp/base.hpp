#pragma once
#include <stdio.h>
#include <stdlib.h>

#include "object.hpp"




typedef Object* (*ZeroArgumentMethod)(Object*);
typedef Object* (*OneArgumentMethod)(Object*, Object*);


namespace rootslot {
  const int program = 0;
  const int console = 1;
  const int string = 2;
  const int integer = 3;
  const int null = 4;

  const int COUNT = 5;
};




struct Heap {
  ObjectPtr rootObject;

};

void* allocateStorage(int byteCount);
Object* allocateObject(int slotCount, int byteCount);

Object* makeObject(int slotCount, int byteCount, const ObjectVTable* vt);
Object* createRootObject();

Object* callMethod(struct Object* obj, int internNumber);
Object* callMethod(struct Object* obj, int internNumber, struct Object* arg1);

char* objectByteStart(struct Object* obj);

extern __thread struct Heap* heap;
const ObjectVTable* get_null_vt();

Object* method_null(struct Object* root);
void init_base(Heap* heap);
Object* get_null_object();
