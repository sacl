
#include "console.hpp"

#include "string.hpp"
#include <stdio.h>

void init_console(Heap* heap) {
  heap->rootObject->slots[rootslot::console] = makeObject(0, 0, get_console_vt());

}


Object* method_print(struct Object* console, struct Object* obj) {
  if (obj->vt == &string_vt) {
    char* str = objectPayloadStart(obj);
    fwrite(str, 1, obj->byteCount, stdout);
  }
  return smallintToObject(0);
}


const ObjectVTable console_vt = {{NULL, NULL, NULL, NULL, (void*)method_print}};


Object* method_console(struct Object*) {
  return heap->rootObject->slots[rootslot::console];
}

const ObjectVTable* get_console_vt() {
  return &console_vt;
}
