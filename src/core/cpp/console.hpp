#pragma once
#include "base.hpp"

extern const ObjectVTable console_vt;

Object* method_console(struct Object*);

const ObjectVTable* get_console_vt();

void init_console(Heap* heap);
