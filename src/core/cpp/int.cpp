


#include "int.hpp"


const ObjectVTable literal_integer_vt = {{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}};

Object* method_integer(struct Object*) {
  return heap->rootObject->slots[rootslot::integer];
}

const ObjectVTable* get_literal_integer_vt() {
  return &literal_integer_vt;
}
