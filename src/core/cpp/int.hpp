#pragma once
#include "base.hpp"


extern const ObjectVTable literal_integer_vt;

Object* method_integer(struct Object*);

const ObjectVTable* get_literal_integer_vt();

