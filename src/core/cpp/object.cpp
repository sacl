
#include "object.hpp"

bool objectIsSmallint(Object* o) {
  return (reinterpret_cast<intptr_t>(o) & 1) == 1;
}

char* objectPayloadStart(Object* o) {
  return reinterpret_cast<char*>(&o->slots[o->slotCount]);
}

Object* smallintToObject(intptr_t i) {
  Object* o = reinterpret_cast<Object*>((i << 1) | 1);
  return o;
}

intptr_t objectToSmallint(Object* o) {
  intptr_t i = reinterpret_cast<intptr_t>(o);
  return i >> 1;
}

void ObjectPtr::set(Object* p) {
  if (ptr == p) return;
  if (ptr) {
    ptr->pinCount--;
  }
  ptr = p;
  if (ptr) {
    ptr->pinCount++;
  }
}
    
