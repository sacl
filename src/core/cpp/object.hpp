#pragma once

#include <stdint.h>


struct VTable {
  void* functions[0];
};

template <int N> struct SizedVTable { void* functions[N]; };


namespace symbol {

  const int Root_0 = 0;
  const int Program_0 = 1;
  const int Start_0 = 2;
  const int Console_0 = 3;
  const int Print_1 = 4;
  const int Size_0 = 5;
  const int At_1 = 6;
  const int From_1 = 7;
  const int Null_0 = 8;

  const int COUNT = 9;

};

typedef SizedVTable<symbol::COUNT> ObjectVTable;

struct Object;
struct Object {
  int flags;
  int pinCount;
  int slotCount;
  int byteCount;
  const ObjectVTable* vt;
  Object* slots[0];
  Object** slotBegin() {return &slots[0];}
  Object** slotEnd() {return &slots[slotCount];}
};


class ObjectPtr {
  Object* ptr;
  void set(Object* p);
public:
  ObjectPtr() : ptr(0) {};
  ObjectPtr(Object* p) : ptr(0) {set(p);};
  ~ObjectPtr() {set(0);}
  const ObjectPtr& operator=(Object* p) {set(p); return *this;}
  operator Object*() {return ptr;}
  Object* operator ->() {return ptr;};
};

bool objectIsSmallint(Object* o);
char* objectPayloadStart(Object* o);
Object* smallintToObject(intptr_t i);
intptr_t objectToSmallint(Object* o);

