#include "base.hpp"
#include "console.hpp"


#include "string.hpp"

Object* method_Program(struct Object* root) {
  return root->slots[rootslot::program];

}

Object* method_start(struct Object* program) {
  Object* o = createString("hello\n");
  callMethod(callMethod(heap->rootObject, symbol::Console_0), symbol::Print_1, o);
  return 0; // Console.writeLine("blah")
}



const ObjectVTable root_vt = {{NULL, (void*)method_Program, NULL, (void*)method_console, NULL,
                               NULL, NULL, NULL, (void*)method_null}};
const ObjectVTable program_vt = {{NULL, NULL, (void*)method_start, NULL, NULL}};

void init_sample(Heap* heap) {
  heap->rootObject->slots[rootslot::program] = makeObject(0, 0, &program_vt);

}

void init_heap(Heap* heap) {
  ObjectPtr root = makeObject(rootslot::COUNT, 0, &root_vt);
  heap->rootObject = root;

  init_base(heap);
  init_sample(heap);

  // call all the modules init functions
  init_console(heap);

}
