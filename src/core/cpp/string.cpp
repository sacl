


#include "string.hpp"

#include <string.h>

Object* method_string_size_0(struct Object* obj) {
  //char* c = objectByteStart(obj);
  return 0; // Console.writeLine("blah")
}

Object* method_string_at_0(struct Object* obj) {
  printf("fixme\n");
  return 0; // Console.writeLine("blah")
}


Object* method_string_from_1(struct Object* console, struct Object* obj) {
  printf("fixme\n");
  return 0; // Console.writeLine("blah")
}


const ObjectVTable string_vt = {{NULL, NULL, NULL, NULL, NULL, (void*)method_string_from_1}};


Object* method_string(struct Object*) {
  return heap->rootObject->slots[rootslot::string];
}

const ObjectVTable* get_string_vt() {
  return &string_vt;
}

Object* createString(const char* str) {
  size_t len = strlen(str);
  Object* strObj = makeObject(0, len, &string_vt);
  memcpy(objectPayloadStart(strObj), str, len);
  return strObj;
}
