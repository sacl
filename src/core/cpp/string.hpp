#pragma once
#include "base.hpp"


extern const ObjectVTable string_vt;

Object* method_string(struct Object*);

const ObjectVTable* get_string_vt();

Object* createString(const char* str);
